# [Verdaccio](https://verdaccio.org/) via Docker Compose

Detta projekt konfigurerar Verdaccio som proxy mot https://registry.npmjs.org/.

Konfigurera npm att använda Verdaccio genom kommandot:
```bash
npm set registry http://localhost:4873
```
